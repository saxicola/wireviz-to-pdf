#!/usr/bin/bash
# Make a printable, landscape pdf for a wiring harness.
# This uses asciidoctor to make the PDF after making an asciidoc template.
# It includes a diagram and a BOM
# The source files are from wireviz
# Run this from the root of Projects/cableing.

# Function to get values from a yaml file
# Requires pyYaml and WireViz. See README
# pip install pyYaml
# Params: filename, key.

# TODO Tiling over several sheets


function yaml() {
    echo  $(python3 -c "import yaml;print(yaml.safe_load(open('$1'))$2)")
}
# Example
#VALUE=$(yaml ~/my_yaml_file.yaml "['a_key']")

if [[ $1 == "" ]] then
    echo You need to supply a source directory containg a YAML file
    exit 1
fi

harness=$1

if [[ ! -d $harness ]] then
    echo Source directory $harness.yml not found!
    exit 2
fi

cd $harness
# Make the source docs from THE source doc
if [[ ! -f loom.yml ]] then
    echo Source file $harness/loom.yml not found!
    exit 3
fi

# Concatenate connector templates and harness file since YAML cannot do includes.
# TODO If no templates are used don't do the concat.
# If a search for "<<: *" comes up empty, don't include templates.
# Better would be selective imports.
# However the processing overhead is minimal anyway.
# grep  "<<: *" loom.yml | awk -F '*' '{print $2}' # returns a list of includes which cand be
# used providing you have a consistent namimg convention. Or maybe a better scanning algorithm.
# Of course as the number of templates increase...
# For the moment just include them all.
# However, if we want our harness to be in a project's source drectory, how do find and
# include the template file?
# Or, still have them here and make links to the sources?
cat ../templates.yml loom.yml > $harness.yml

# Process the YAML
wireviz $harness.yml || {
    printf "\nwireviz failed with error messages."
    exit 4
}

outfile=$harness.adoc
echo = $harness > $outfile
echo :pdf-page-layout: landscape >> $outfile
echo >> $outfile

# There are metadata in the YAML file that's unused.  This could be added to the asciidoc.
# Missing keys will report an error but this can be safely ignored.
yaml $harness.yml  "['description']" >> $outfile
echo >> $outfile


cat $harness.tsv > tmpfile
# Make the separator | from tabs
sed -i 's/\t/|/g' tmpfile
sed -i 's/^/|/g' tmpfile
# Shorten the headers a little.
sed -i 's/Designators/Ref./g' tmpfile
sed -i 's/Manufacturer/Manf./g' tmpfile

echo .Bill of Materials >> $outfile
# Number of columns can vary depending on how a connector is defined.
# Count the column separators in the heading
cols=$(head -1 tmpfile | grep -o '|' | wc -l)
# then act appropriately
if [[ $cols == 5 ]] then
    echo '[options="header",cols="1,1,1,4,1"]' >> $outfile
elif [[ $cols == 7 ]] then
    echo '[options="header",cols="1,1,1,4,1,1,1"]' >> $outfile
else
    echo '[options="header",cols="1,1,1,4,1,2,1,1,1"]' >> $outfile
fi
# Add the table to the asciidoc
echo '|=======================' >> $outfile
cat tmpfile >> $outfile
echo '|=======================' >> $outfile
#echo >> $outfile

# Add this wiring image to the asciidoc.
echo image:$harness.svg[$harness,width=1000] >> $outfile
#echo >> $outfile


# Remove temporary files.
rm tmpfile
rm $harness.yml

# Convert to PDF
asciidoctor-pdf $harness.adoc

cat  $outfile
# Display the file.
okular $harness.pdf
