from wireviz import wireviz


# Sample code.
# Read YAML file
f = wireviz.open_file_read("loom.yml")
yaml_data = f.read()
f.close()
# This works.

# For some reason we have to create a temporary file?
f = open('loom.tmp.svg','w')
f.close()
wireviz.parse('loom.yml', output_formats=("svg"))
# Resulting file is empty!
# And so endeth this lesson.
